from src.nodos import Nodo

# Definición de la clase 'ListaLigada':
class ListaLigada:
    def __init__(self):
        # Constructor, inicializa una lista ligada vacía
        self.cabeza = None

    def agregar_elemento(self, valor):
        # Agrega un elemento al final de la lista
        nuevo_nodo = Nodo(valor)  # se crea un nuevo nodo con el valor dado
        if self.cabeza is None:  # Si la lista está vacía, el nuevo nodo se convierte en la cabeza
            self.cabeza = nuevo_nodo
        else:
            actual = self.cabeza
            while actual.siguiente:
                actual = actual.siguiente
            actual.siguiente = nuevo_nodo  # Se recorre la lista hasta el último nodo y enlaza el nuevo nodo

    def buscar_elemento(self, valor):
        # Busca un elemento en la lista y devuelve True si lo encuentra, sino, False
        actual = self.cabeza
        while actual:
            if actual.valor == valor:
                return True
            actual = actual.siguiente
        return False

    def elimina_cabeza(self):
        # Elimina el primer elemento de la lista (cabeza)
        if self.cabeza:
            self.cabeza = self.cabeza.siguiente

    def elimina_rabo(self):
        # Elimina el último elemento de la lista (rabo)
        if self.cabeza is None:
            return
        actual = self.cabeza
        if actual.siguiente is None:  # Si la lista tiene solo un elemento
            self.cabeza = None
            return
        while actual.siguiente.siguiente:
            actual = actual.siguiente
        actual.siguiente = None  # se recorre la lista hasta el penúltimo nodo y se elimina la referencia al último

    def tamagno(self):
        # Devuelve el tamaño de la lista
        tamaño = 0
        actual = self.cabeza
        while actual:
            tamaño += 1
            actual = actual.siguiente
        return tamaño

    def copia(self):
        # Crea una copia de una lista
        nueva_lista = ListaLigada()  # Crea una nueva lista vacía
        actual = self.cabeza
        while actual:
            nueva_lista.agregar_elemento(actual.valor)  # Agrega los elementos
            actual = actual.siguiente
        return nueva_lista

    def __str__(self):
        # Devuelve una representación en string de la lista
        valores = []
        actual = self.cabeza
        while actual:
            valores.append(str(actual.valor))
            actual = actual.siguiente
        return " -> ".join(valores)
